import {APP_INITIALIZER, NgModule} from '@angular/core';
import {BrowserModule} from '@angular/platform-browser';
import {RouteReuseStrategy} from '@angular/router';
import {HttpClientModule} from '@angular/common/http';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';

import {IonicModule, IonicRouteStrategy} from '@ionic/angular';
import {IonicStorageModule} from '@ionic/storage';
import {SplashScreen} from '@ionic-native/splash-screen/ngx';
import {StatusBar} from '@ionic-native/status-bar/ngx';

import {AppRoutingModule} from './app-routing.module';

import {HttpService} from './shared/services/http.service';
import {AuthService} from './auth/auth.service';
import {MyUserService} from './shared/services/my-user.service';
import {TranslateService} from './shared/translation/translate.service';
import {StorageService} from './shared/services/storage.service';

import {AppComponent} from './app.component';
import {CinemaService} from './home/cinema/services/cinema.service';
import {MovieTicketsService} from './home/cinema/services/movie-tickets.service';
import {HomepageService} from './home/start/homepage.service';
import {LoadingService} from './shared/services/loading.service';

@NgModule({
  declarations: [AppComponent],
  entryComponents: [],
  imports: [
    BrowserModule,
    IonicModule.forRoot(),
    IonicStorageModule.forRoot(),
    AppRoutingModule,
    BrowserAnimationsModule,
    HttpClientModule
  ],
  providers: [
    StatusBar,
    SplashScreen,
    StorageService,
    AuthService,
    HttpService,
    MyUserService,
    CinemaService,
    MovieTicketsService,
    HomepageService,
    LoadingService,
    {
      provide: APP_INITIALIZER,
      useFactory: setupTranslateFactory,
      deps: [TranslateService],
      multi: true
    },
    {provide: RouteReuseStrategy, useClass: IonicRouteStrategy},
  ],
  bootstrap: [AppComponent]
})
export class AppModule {
}

export function setupTranslateFactory(
  // tslint:disable-next-line:ban-types
  service: TranslateService): Function {
  return () => service.use('DEFAULT');
}
