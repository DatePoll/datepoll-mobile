import {Injectable} from '@angular/core';
import {Storage} from '@ionic/storage';

@Injectable({
  providedIn: 'root'
})
export class StorageService {

  constructor(private storage: Storage) {
  }

  public async getValueWithKey(key: string): Promise<any> {
    return await this.storage.get(key);
  }

  public setValueWithKey(key: string, value: any) {
    return this.storage.set(key, value);
  }
}
