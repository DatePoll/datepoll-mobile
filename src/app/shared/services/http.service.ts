/* tslint:disable:object-literal-shorthand */
import {Injectable} from '@angular/core';
import {StorageService} from './storage.service';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {AuthService} from '../../auth/auth.service';
import {retry} from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HttpService {

  private apiUrl;

  constructor(private storageService: StorageService, private authService: AuthService, private http: HttpClient) {
    this.refreshApiUrl();
  }

  public refreshApiUrl() {
    this.storageService.getValueWithKey('apiUrl').then((value) => {
      this.apiUrl = value;
    });
  }

  public loggedInV1GETRequest(url: string, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      return this.loggedInV1GETRequest(url, functionUser);
    } else {
      this.log('GET', url, functionUser);

      const token = this.authService.getJWTToken(functionUser);

      return this.http.get(this.apiUrl + '/v1' + url + '?token=' + token).pipe(
        retry(3)
      );
    }
  }

  public loggedInV1POSTRequest(url: string, body: any, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      return this.loggedInV1POSTRequest(url, body, functionUser);
    } else {
      this.log('POST', url, functionUser);

      const token = this.authService.getJWTToken(functionUser);
      const headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this.http.post(this.apiUrl + '/v1' + url + '?token=' + token, body, {headers: headers}).pipe(
        retry(3)
      );
    }
  }

  public loggedInV1PUTRequest(url: string, body: any, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      return this.loggedInV1PUTRequest(url, body, functionUser);
    } else {
      this.log('PUT', url, functionUser);

      const token = this.authService.getJWTToken(functionUser);
      const headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this.http.put(this.apiUrl + '/v1' + url + '?token=' + token, body, {headers: headers}).pipe(
        retry(3)
      );
    }
  }

  public loggedInV1DELETERequest(url: string, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      return this.loggedInV1DELETERequest(url, functionUser);
    } else {
      this.log('DELETE', url, functionUser);

      const token = this.authService.getJWTToken(functionUser);
      return this.http.delete(this.apiUrl + '/v1' + url + '?token=' + token).pipe(
        retry(3)
      );
    }
  }

  public getSettingRequest(url: string, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      return this.getSettingRequest(url, functionUser);
    } else {
      if (functionUser != null) {
        console.log('getSettingsRequest | ' + functionUser);
      }
      return this.http.get(this.apiUrl + '/settings' + url).pipe(
        retry(3)
      );
    }
  }

  public setSettingsRequest(url: string, body: any, functionUser: string = null) {
    if (this.apiUrl == null) {
      this.refreshApiUrl();
      this.setSettingsRequest(url, body, functionUser);
    } else {
      const token = this.authService.getJWTToken(functionUser);
      const headers = new HttpHeaders().set('Content-Type', 'application/json');

      return this.http.post(this.apiUrl + '/settings/administration' + url + '?token=' + token, body, {headers: headers}).pipe(
        retry(3)
      ).subscribe(
        (response: any) => {
          console.log(response);
        },
        (error) => console.log(error)
      );
    }
  }

  private log(type: string, url: string, functionUser: string = null) {
    if (functionUser != null) {
      console.log('httpService | ' + type + ' Request | URL: ' + url + ' | Function user: ' + functionUser);
    } else {
      console.log('httpService | ' + type + ' Request | URL: ' + url);
    }
  }
}
