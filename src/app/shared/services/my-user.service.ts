import {Injectable} from '@angular/core';
import {HttpService} from './http.service';
import {Subject} from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class MyUserService {
  public ID: number;

  public title: string;

  public firstname: string;
  public surname: string;
  public username: string;

  public emailAddresses: string[];

  public streetname: string;
  public streetnumber: string;
  public zipcode: number;
  public location: string;

  public birthday: Date;
  public joindate: Date;

  public changed: Subject<null> = new Subject<null>();

  constructor(private httpService: HttpService) {
    this.fetchMyself();
  }

  fetchMyself() {
    this.httpService.loggedInV1GETRequest('/user/myself', 'fetchMyself').subscribe(
      (dataComplete: any) => {
        console.log(dataComplete);

        const data = dataComplete.user;
        this.ID = data.id;
        this.title = data.title;
        this.firstname = data.firstname;
        this.surname = data.surname;
        this.username = data.username;
        this.emailAddresses = data.emailAddresses;
        this.streetname = data.streetname;
        this.streetnumber = data.streetnumber;
        this.zipcode = data.zipcode;
        this.location = data.location;
        this.birthday = data.birthday;
        this.joindate = data.join_date;

        this.changed.next(null);
      },
      (error) => console.log(error)
    );
  }
}
