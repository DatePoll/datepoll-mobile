import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';
import {StorageService} from '../services/storage.service';

@Injectable({
  providedIn: 'root'
})
export class TranslateService {

  data: any = {};

  constructor(private http: HttpClient, private storageService: StorageService) {
  }

  getStringForValue(key): string {
    return this.data[key];
  }

  use(lang: string): Promise<{}> {

    if (lang === 'DEFAULT') {
      this.storageService.getValueWithKey('language').then((language) => {
        if (language == null) {
          this.storageService.setValueWithKey('language', 'de');
          lang = 'de';
          console.log('No language in storage found! Using de as default');
        } else {
          lang = language;
          console.log('Language in storage found! Using ' + lang);
        }

        return new Promise<{}>((resolve, reject) => {
          const langPath = `assets/i18n/${lang || 'de'}.json`;

          this.http.get<{}>(langPath).subscribe(
            translation => {
              this.data = Object.assign({}, translation || {});
              resolve(this.data);
            },
            error => {
              this.data = {};
              resolve(this.data);
            }
          );
        });
      });
    } else {
      console.log('Language changed to ' + lang);
      this.storageService.setValueWithKey('language', lang);

      return new Promise<{}>((resolve, reject) => {
        const langPath = `assets/i18n/${lang || 'de'}.json`;

        this.http.get<{}>(langPath).subscribe(
          translation => {
            this.data = Object.assign({}, translation || {});
            resolve(this.data);
          },
          error => {
            this.data = {};
            resolve(this.data);
          }
        );
      });
    }
  }
}
