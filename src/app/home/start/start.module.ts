import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {StartComponent} from './start.component';
import {TranslationModule} from '../../shared/translation/translation.module';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslationModule,
    RouterModule.forChild([{path: '', component: StartComponent}])
  ],
  declarations: [StartComponent]
})
export class StartModule {
}
