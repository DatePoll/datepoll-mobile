import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

import {HttpService} from '../../shared/services/http.service';

import {HomeBirthdayModel} from './birthdays.model';
import {HomeBookingsModel} from './bookings.model';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  private birthdays: HomeBirthdayModel[] = [];
  public birthdaysChange: Subject<HomeBirthdayModel[]> = new Subject<HomeBirthdayModel[]>();

  private bookings: HomeBookingsModel[] = [];
  public bookingsChange: Subject<HomeBookingsModel[]> = new Subject<HomeBookingsModel[]>();

  // private _events: Event[] = [];
  // public eventsChange: Subject<Event[]> = new Subject<Event[]>();

  constructor(private httpService: HttpService) {
  }

  private setBirthdays(birthdays: HomeBirthdayModel[]) {
    this.birthdays = birthdays;
    this.birthdaysChange.next(this.birthdays.slice());
  }

  public getBirthdays(): HomeBirthdayModel[] {
    this.fetchData();
    return this.birthdays.slice();
  }

  public setBookings(bookings: HomeBookingsModel[]) {
    this.bookings = bookings;
    this.bookingsChange.next(this.bookings.slice());
  }

  public getBookings(): HomeBookingsModel[] {
    this.fetchData();
    return this.bookings.slice();
  }

  // private setEvents(events: Event[]) {
  //   this._events = events;
  //   this.eventsChange.next(this._events.slice());
  // }
  //
  // public getEvents(): Event[] {
  //   this.fetchData();
  //   return this._events.slice();
  // }

  public fetchData(refreshSpinner = null) {
    this.httpService.loggedInV1GETRequest('/user/homepage', 'fetchHomepageData').subscribe(
      (data: any) => {
        console.log(data);

        const bookings = data.bookings;

        const bookingsToSave = [];
        for (const booking of bookings) {
          bookingsToSave.push(new HomeBookingsModel(booking.movieID, booking.movieName, booking.amount, booking.movieDate,
            booking.workerID, booking.workerName, booking.emergencyWorkerID, booking.emergencyWorkerName));
        }

        this.setBookings(bookingsToSave);

        const birthdays = data.birthdays;

        const birthdaysToSave = [];
        for (const birthday of birthdays) {
          birthdaysToSave.push(new HomeBirthdayModel(birthday.name, birthday.date));
        }

        this.setBirthdays(birthdaysToSave);

        // const events = data.events;
        // const eventsToSave = [];
        // for (let i = 0; i < events.length; i++) {
        //   const fetchedEvent = events[i];
        //   const event = new Event(fetchedEvent.id, fetchedEvent.name, new Date(fetchedEvent.startDate), new Date(fetchedEvent.endDate),
        //     fetchedEvent.forEveryone, fetchedEvent.description, fetchedEvent.decisions);
        //   event.alreadyVotedFor = fetchedEvent.alreadyVoted;
        //   eventsToSave.push(event);
        // }
        // this.setEvents(eventsToSave);

        if (refreshSpinner != null) {
          refreshSpinner.target.complete();
        }
      },
      (error) => console.log(error)
    );
  }
}
