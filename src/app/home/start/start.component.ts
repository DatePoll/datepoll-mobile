import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';

import {HomepageService} from './homepage.service';

import {HomeBookingsModel} from './bookings.model';
import {HomeBirthdayModel} from './birthdays.model';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.scss'],
})
export class StartComponent implements OnDestroy {

  birthdays: HomeBirthdayModel[];
  birthdaysSubscription: Subscription;

  bookings: HomeBookingsModel[];
  bookingsSubscription: Subscription;

  constructor(private homePageService: HomepageService) {
    this.birthdays = this.homePageService.getBirthdays();
    this.birthdaysSubscription = this.homePageService.birthdaysChange.subscribe((value) => {
      this.birthdays = value;
    });

    this.bookings = this.homePageService.getBookings();
    this.bookingsSubscription = this.homePageService.bookingsChange.subscribe((value) => {
      this.bookings = value;
    });
  }

  ngOnDestroy(): void {
    this.birthdaysSubscription.unsubscribe();
    this.bookingsSubscription.unsubscribe();
  }

  onRefresh(refreshSpinner) {
    this.homePageService.fetchData(refreshSpinner);
  }

}
