import {Component, OnDestroy, OnInit} from '@angular/core';
import {MenuController} from '@ionic/angular';
import {Subscription} from 'rxjs';

import {MyUserService} from '../shared/services/my-user.service';
import {TranslateService} from '../shared/translation/translate.service';
import {AuthService} from '../auth/auth.service';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.scss'],
})
export class HomeComponent implements OnInit, OnDestroy {
  current = this.translate.getStringForValue('START');
  name: string;

  userSubscription: Subscription;

  constructor(private menu: MenuController,
              private myUserService: MyUserService,
              private translate: TranslateService,
              private authService: AuthService) {
    this.name = this.myUserService.firstname;
    this.userSubscription = this.myUserService.changed.subscribe((value) => {
      this.name = this.myUserService.firstname;
    });
  }

  ngOnInit() {
    this.menu.enable(true, 'sidenav');
  }

  ngOnDestroy(): void {
    this.userSubscription.unsubscribe();
  }

  clickedNavButton(current: string) {
    this.menu.close('sidenav');
    this.current = this.translate.getStringForValue(current);
  }

  logout() {
    this.authService.logout();
  }
}
