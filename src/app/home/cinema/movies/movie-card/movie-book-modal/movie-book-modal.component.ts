import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';
import {NgForm} from '@angular/forms';

import {HttpService} from '../../../../../shared/services/http.service';
import {MyUserService} from '../../../../../shared/services/my-user.service';
import {CinemaService} from '../../../services/cinema.service';
import {Movie} from '../../../models/movie.model';

@Component({
  selector: 'app-movie-book-modal',
  templateUrl: './movie-book-modal.component.html',
  styleUrls: ['./movie-book-modal.component.scss'],
})
export class MovieBookModalComponent implements OnInit {
  @Input()
  movie: Movie;

  freeTickets: number;

  constructor(private httpService: HttpService,
              private myUserService: MyUserService,
              private modalController: ModalController,
              private cinemaService: CinemaService) {
  }

  ngOnInit() {
    this.freeTickets = 20 - this.movie.bookedTickets;
  }

  bookTickets(form: NgForm) {
    const amount = form.controls.amount.value;
    if (this.freeTickets - amount < 0) {
      return;
    }

    const bookingObject = {
      'movie_id': this.movie.id,
      'ticketAmount': amount
    };

    this.httpService.loggedInV1POSTRequest('/cinema/booking', bookingObject, 'bookTickets').subscribe(
      (data: any) => {
        console.log(data);
        this.movie.bookedTickets += amount;
        this.movie.bookedTicketsForYourself += amount;
      },
      (error) => {
        console.log(error);
        this.cinemaService.fetchNotShownMovies();
      }
    );
    this.dismissModal();
  }

  async dismissModal() {
    await this.modalController.dismiss();
  }
}
