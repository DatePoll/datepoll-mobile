import {Component, Input, OnInit} from '@angular/core';
import {ModalController} from '@ionic/angular';

import {CinemaService} from '../../services/cinema.service';
import {HttpService} from '../../../../shared/services/http.service';
import {MyUserService} from '../../../../shared/services/my-user.service';

import {Movie} from '../../models/movie.model';
import {MovieBookModalComponent} from './movie-book-modal/movie-book-modal.component';

@Component({
  selector: 'app-movie-card',
  templateUrl: './movie-card.component.html',
  styleUrls: ['./movie-card.component.scss'],
})
export class MovieCardComponent implements OnInit {
  @Input()
  movie: Movie;

  soldOut: boolean;

  myUserService: MyUserService;

  constructor(private cinemaService: CinemaService,
              myUserService: MyUserService,
              private modalController: ModalController,
              private httpService: HttpService) {
    this.myUserService = myUserService;
  }

  ngOnInit(): void {
    this.soldOut = this.movie.bookedTickets >= 20;
  }

  async bookTickets() {
    const modal = await this.modalController.create({
      component: MovieBookModalComponent,
      componentProps: {
        'movie': this.movie
      }
    });
    modal.present();
  }

  cancelTickets(element: any) {
    element.disabled = true;
    this.httpService.loggedInV1DELETERequest('/cinema/booking/' + this.movie.id, 'cancelTickets').subscribe(
      (data: any) => {
        console.log(data);
        this.movie.bookedTickets -= this.movie.bookedTicketsForYourself;
        this.movie.bookedTicketsForYourself = 0;
      },
      (error) => {
        console.log(error);
        this.cinemaService.fetchNotShownMovies();
      }
    );
  }

  applyForWorker(element) {
    element.disabled = true;
    this.cinemaService.applyForWorker(this.movie);
  }

  signOutForWorker(element) {
    element.disabled = true;
    this.cinemaService.signOutForWorker(this.movie);
  }

  applyForEmergencyWorker(element) {
    element.disabled = true;
    this.cinemaService.applyForEmergencyWorker(this.movie);
  }

  signOutForEmergencyWorker(element) {
    element.disabled = true;
    this.cinemaService.signOutForEmergencyWorker(this.movie);
  }

}
