import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';

import {CinemaService} from '../services/cinema.service';
import {Movie} from '../models/movie.model';

@Component({
  selector: 'app-movies',
  templateUrl: './movies.component.html',
  styleUrls: ['./movies.component.scss'],
})
export class MoviesComponent implements OnDestroy {
  movies: Movie[];
  moviesCopy: Movie[];
  moviesSubscription: Subscription;

  constructor(private cinemaService: CinemaService) {
    this.movies = this.cinemaService.getNotShownMovies();
    this.moviesCopy = this.movies.slice();
    this.moviesSubscription = this.cinemaService.notShownMoviesChange.subscribe((value) => {
      this.movies = value;
      this.moviesCopy = this.movies.slice();
    });
  }

  ngOnDestroy(): void {
    this.moviesSubscription.unsubscribe();
  }

  onRefresh(refreshSpinner) {
    this.cinemaService.fetchNotShownMovies(refreshSpinner);
  }

  onSearch(event) {
    const searchKeyword = event.detail.value;
    this.movies = [];
    for (const movie of this.moviesCopy) {
      if (movie.name.toUpperCase().includes(searchKeyword.toUpperCase())) {
        this.movies.push(movie);
      }
    }
  }
}
