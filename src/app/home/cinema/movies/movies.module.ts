import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TranslationModule} from '../../../shared/translation/translation.module';

import {MoviesComponent} from './movies.component';
import {MovieCardComponent} from './movie-card/movie-card.component';
import {MovieBookModalComponent} from './movie-card/movie-book-modal/movie-book-modal.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslationModule,
    RouterModule.forChild([{path: '', component: MoviesComponent}])
  ],
  declarations: [
    MoviesComponent,
    MovieCardComponent,
    MovieBookModalComponent
  ],
  entryComponents: [
    MovieBookModalComponent
  ]
})
export class MoviesModule {
}
