import {IonicModule} from '@ionic/angular';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TranslationModule} from '../../shared/translation/translation.module';
import {CinemaRoutingModule} from './cinema.router.module';

import {CinemaComponent} from './cinema.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslationModule,
    CinemaRoutingModule
  ],
  declarations: [
    CinemaComponent
  ]
})
export class CinemaModule {
}
