import {IonicModule} from '@ionic/angular';
import {RouterModule} from '@angular/router';
import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';

import {TranslationModule} from '../../../shared/translation/translation.module';

import {WorkerComponent} from './worker.component';
import {MovieOrderComponent} from './movie-order/movie-order.component';

@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslationModule,
    RouterModule.forChild([{path: '', component: WorkerComponent}])
  ],
  declarations: [
    WorkerComponent,
    MovieOrderComponent
  ]
})
export class WorkerModule {
}
