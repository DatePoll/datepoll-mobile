import {Component, OnDestroy} from '@angular/core';
import {Subscription} from 'rxjs';

import {MovieOrder, MovieTicketsService} from '../services/movie-tickets.service';

@Component({
  selector: 'app-worker',
  templateUrl: './worker.component.html',
  styleUrls: ['./worker.component.scss'],
})
export class WorkerComponent implements OnDestroy {
  loading = true;
  movieOrders: MovieOrder[];
  movieOrdersCopy: MovieOrder[];
  movieOrdersSubscription: Subscription;

  constructor(private movieTicketsService: MovieTicketsService) {
    this.movieOrders = this.movieTicketsService.getMovieOrders();
    this.movieOrdersCopy = this.movieOrders.slice();
    if (this.movieOrders.length > 0) {
      this.loading = false;
    }
    this.movieOrdersSubscription = this.movieTicketsService.movieOrdersChange.subscribe((value) => {
      this.movieOrders = value;
      this.movieOrdersCopy = this.movieOrders.slice();
      this.loading = false;
    });
  }

  ngOnDestroy(): void {
    this.movieOrdersSubscription.unsubscribe();
  }

  onRefresh(refreshSpinner) {
    this.movieTicketsService.fetchMovieOrders(refreshSpinner);
  }

  onSearch(event) {
    const searchKeyword = event.detail.value;
    this.movieOrders = [];
    for (const movieOrder of this.movieOrdersCopy) {
      if (movieOrder.movieName.toUpperCase().includes(searchKeyword.toUpperCase())) {
        this.movieOrders.push(movieOrder);
      }
    }
  }
}
