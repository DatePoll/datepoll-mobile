import {Component, Input, OnInit} from '@angular/core';
import {MovieOrder, TicketOrder} from '../../services/movie-tickets.service';

@Component({
  selector: 'app-movie-order',
  templateUrl: './movie-order.component.html',
  styleUrls: ['./movie-order.component.scss'],
})
export class MovieOrderComponent implements OnInit {
  @Input() movieOrder: MovieOrder;

  movieName: string;
  movieDate: Date;
  ticketOrders: TicketOrder[];
  ticketsTotalAmount = 0;

  constructor() {}

  ngOnInit() {
    this.movieName = this.movieOrder.movieName;
    this.movieDate = this.movieOrder.date;
    this.ticketOrders = this.movieOrder.getTicketOrders();
    for (const ticketOrder of this.ticketOrders) {
      this.ticketsTotalAmount += ticketOrder.amount;
    }
  }

}
