import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

import {HttpService} from '../../../shared/services/http.service';
import {MyUserService} from '../../../shared/services/my-user.service';

import {Movie} from '../models/movie.model';
import {MovieTicketsService} from './movie-tickets.service';

@Injectable({
  providedIn: 'root'
})
export class CinemaService {

  public notShownMoviesChange: Subject<Movie[]> = new Subject<Movie[]>();
  private notShownMovies: Movie[] = [];

  constructor(private httpService: HttpService, private myUserService: MyUserService, private movieTicketsService: MovieTicketsService) {
  }

  public getNotShownMovies(): Movie[] {
    this.fetchNotShownMovies();
    return this.notShownMovies.slice();
  }

  public setNotShownMovies(movies: Movie[]) {
    this.notShownMovies = movies;
    this.notShownMoviesChange.next(this.notShownMovies.slice());
  }

  public fetchNotShownMovies(refreshSpinner = null) {
    this.httpService.loggedInV1GETRequest('/cinema/notShownMovies', 'fetchNotShownMovies').subscribe(
      (data: any) => {
        console.log(data);

        const fetchedMovies = data.movies;

        const movies = [];
        for (const movie of fetchedMovies) {
          let workerID = -1;
          let emergencyWorkerID = -1;

          if (movie.workerID != null) {
            workerID = movie.workerID;
          }

          if (movie.emergencyWorkerID != null) {
            emergencyWorkerID = movie.emergencyWorkerID;
          }

          const date = new Date(movie.date);
          const localMovie = new Movie(movie.id, movie.name, date, movie.trailerLink, movie.posterLink, workerID, movie.workerName,
            emergencyWorkerID, movie.emergencyWorkerName, movie.bookedTickets, movie.movie_year_id);
          localMovie.bookedTicketsForYourself = movie.bookedTicketsForYourself;
          movies.push(localMovie);
        }
        this.setNotShownMovies(movies);
        if (refreshSpinner != null) {
          refreshSpinner.target.complete();
        }
      },
      (error) => console.log(error)
    );
  }

  public applyForWorker(movie: Movie) {
    this.httpService.loggedInV1POSTRequest('/cinema/worker/' + movie.id, {}, 'applyForWorker').subscribe(
      (data: any) => {
        console.log(data);
        movie.workerID = this.myUserService.ID;
        movie.workerName = this.myUserService.firstname + ' ' + this.myUserService.surname;
        this.movieTicketsService.fetchMovieOrders();
      },
      (error) => {
        console.log(error);
        this.fetchNotShownMovies();
      }
    );
  }

  public signOutForWorker(movie: Movie) {
    this.httpService.loggedInV1DELETERequest('/cinema/worker/' + movie.id, 'signOutForWorker').subscribe(
      (data: any) => {
        console.log(data);
        movie.workerName = null;
        movie.workerID = -1;
        this.movieTicketsService.fetchMovieOrders();
      },
      (error) => {
        console.log(error);
        this.fetchNotShownMovies();
      }
    );
  }

  public applyForEmergencyWorker(movie: Movie) {
    this.httpService.loggedInV1POSTRequest('/cinema/emergencyWorker/' + movie.id, {}, 'applyForEmergencyWorker').subscribe(
      (data: any) => {
        console.log(data);
        movie.emergencyWorkerID = this.myUserService.ID;
        movie.emergencyWorkerName = this.myUserService.firstname + ' ' + this.myUserService.surname;
        this.movieTicketsService.fetchMovieOrders();
      },
      (error) => {
        console.log(error);
        this.fetchNotShownMovies();
      }
    );
  }

  public signOutForEmergencyWorker(movie: Movie) {
    this.httpService.loggedInV1DELETERequest('/cinema/emergencyWorker/' + movie.id, 'signOutForEmergencyWorker').subscribe(
      (data: any) => {
        console.log(data);
        movie.emergencyWorkerName = null;
        movie.emergencyWorkerID = -1;
        this.movieTicketsService.fetchMovieOrders();
      },
      (error) => {
        console.log(error);
        this.fetchNotShownMovies();
      }
    );
  }
}
