import {Injectable} from '@angular/core';
import {Subject} from 'rxjs';

import {HttpService} from '../../../shared/services/http.service';

@Injectable({
  providedIn: 'root'
})
export class MovieTicketsService {
  private movieOrders: MovieOrder[] = [];
  public movieOrdersChange: Subject<MovieOrder[]> = new Subject<MovieOrder[]>();

  constructor(private httpService: HttpService) {
  }

  public setMovieOrders(movieOrders: MovieOrder[]) {
    this.movieOrders = movieOrders;
    this.movieOrdersChange.next(this.movieOrders.slice());
  }

  public getMovieOrders(): MovieOrder[] {
    this.fetchMovieOrders();
    return this.movieOrders.slice();
  }

  public fetchMovieOrders(refreshSpinner = null) {
    this.httpService.loggedInV1GETRequest('/cinema/worker', 'fetchMovieOrders').subscribe(
      (completeData: any) => {
        console.log(completeData);
        const movies = completeData.movies;

        const movieOrdersToSave = [];
        for (const movie of movies) {
          const localMovieOrder = new MovieOrder(movie.movieID, movie.movieName, movie.date);

          const ticketOrdersToSave = [];
          for (const order of movie.orders) {
            ticketOrdersToSave.push(new TicketOrder(order.userID, order.userName, order.amount));
          }
          localMovieOrder.setTicketOrders(ticketOrdersToSave);
          movieOrdersToSave.push(localMovieOrder);
        }

        this.setMovieOrders(movieOrdersToSave);
        if (refreshSpinner != null) {
          refreshSpinner.target.complete();
        }
      },
      (error1) => console.log(error1)
    );
  }
}

export class MovieOrder {
  public movieID: number;
  public readonly movieName: string;
  public readonly date: Date;

  private ticketOrders: TicketOrder[] = [];

  constructor(movieID: number, movieName: string, date: Date) {
    this.movieID = movieID;
    this.movieName = movieName;
    this.date = date;
  }

  public setTicketOrders(ticketOrders: TicketOrder[]) {
    this.ticketOrders = ticketOrders;
  }

  public getTicketOrders(): TicketOrder[] {
    return this.ticketOrders.slice();
  }
}

export class TicketOrder {
  public readonly userID: number;
  public readonly userName: string;
  public readonly amount: number;

  constructor(userID: number, userName: string, amount: number) {
    this.userID = userID;
    this.userName = userName;
    this.amount = amount;
  }
}
