import {NgModule} from '@angular/core';
import {RouterModule, Routes} from '@angular/router';
import {HomeComponent} from './home.component';

const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
    children: [
      {
        path: 'start',
        children: [
          {
            path: '',
            loadChildren: () => import('../home/start/start.module').then(m => m.StartModule)
          }
        ]
      },
      {
        path: 'cinema',
        children: [
          {
            path: '',
            loadChildren: () => import('../home/cinema/cinema.module').then(m => m.CinemaModule)
          }
        ]
      },
      {
        path: 'tab3',
        children: [
          {
            path: '',
            loadChildren: './tab3/tab3.module#Tab3PageModule'
          }
        ]
      },
      {
        path: '',
        redirectTo: '/home/start',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/home/start',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [
    RouterModule.forChild(routes)
  ],
  exports: [RouterModule]
})
export class HomeRoutingModule {
}
