import {Component} from '@angular/core';
import {MyUserService} from '../shared/services/my-user.service';

@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {

  constructor(private myUserService: MyUserService) {
    this.myUserService.fetchMyself();
  }

}
