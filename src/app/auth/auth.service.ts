/* tslint:disable:object-literal-key-quotes object-literal-shorthand */
import {Injectable} from '@angular/core';
import {HttpClient, HttpHeaders} from '@angular/common/http';
import {NavController, ToastController} from '@ionic/angular';

import {StorageService} from '../shared/services/storage.service';
import {TranslateService} from '../shared/translation/translate.service';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  private sessionToken: string = null;
  private jwtToken: string = null;

  constructor(
    private http: HttpClient,
    private nav: NavController,
    private translate: TranslateService,
    private toastController: ToastController,
    private storageService: StorageService) {
    this.getSessionToken();
  }

  public signinUser(username: string, password: string, apiUrl: string) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    const signInObject = {
      'username': username,
      'password': password,
      'stayLoggedIn': true,
      'sessionInformation': 'DatePoll-Mobile'
    };

    return this.http.post(apiUrl + '/auth/signin', signInObject, {headers: headers});
  }

  public performLogin(token: string, sessionToken: string) {
    this.setSessionToken(sessionToken);
    this.setJWTToken(token);

    console.log('signIn | token: ' + token);
    this.nav.navigateRoot('/home');
  }

  public setJWTToken(token: string) {
    this.jwtToken = token;
  }

  public getJWTToken(functionUser: string): string {
    this.refreshJWTToken();
    console.log('authService | ' + functionUser + ' | GET JWT Token: ' + this.jwtToken);
    return this.jwtToken;
  }

  public refreshJWTToken(navigate = null) {
    console.log('authService | Refreshing JWT Token..');

    this.storageService.getValueWithKey('sessionToken').then((value) => {
      this.sessionToken = value;

      if (this.doesSessionTokenExists()) {
        const dto = {
          'sessionToken': this.getSessionToken(),
          'sessionInformation': 'DatePoll-Mobile'
        };

        this.storageService.getValueWithKey('apiUrl').then((apiUrl) => {
          this.http.post(apiUrl + '/auth/IamLoggedIn', dto).subscribe(
            (response: any) => {
              console.log(response);
              this.setJWTToken(response.token);
              console.log('authService | JWT Token refreshed successful');

              if (navigate != null) {
                this.nav.navigateRoot(navigate);
              }
            },
            (error) => {
              console.log(error);
              this.setSessionToken(null);
              this.setJWTToken(null);
              this.nav.navigateRoot('/signin');
            }
          );
        });
      } else {
        console.log('authService | Tried to refresh token but there is no Session token => Routing to signin');
        this.nav.navigateRoot('/signin');
      }
    });
  }

  public setSessionToken(token: string) {
    this.storageService.setValueWithKey('sessionToken', token);
    this.sessionToken = token;
  }

  public getSessionToken(): string {
    if (this.sessionToken == null) {
      this.storageService.getValueWithKey('sessionToken').then((value) => {
        this.sessionToken = value;
        console.log('authService | Get session token: ' + this.sessionToken);
      });
    }
    return this.sessionToken;
  }

  public checkIfSessionTokenExistsAsync() {
    return this.storageService.getValueWithKey('sessionToken');
  }

  public logout() {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');
    const object = {
      'sessionToken': this.sessionToken
    };

    this.storageService.getValueWithKey('apiUrl').then((apiUrl) => {
      this.http.post(apiUrl + '/v1/user/myself/session/logoutCurrentSession?token=' + this.jwtToken, object,
        {headers: headers}).subscribe(
        (data: any) => {
          console.log(data);
        },
        (error) => console.log(error)
      );

      this.setJWTToken(null);
    });

    this.setSessionToken(null);
    this.nav.navigateRoot('/signin');
  }

  private doesSessionTokenExists(): boolean {
    return !(this.sessionToken == null);
  }

  public changePasswordAfterSignin(username: string, oldPassword: string, newPassword: string) {
    const headers = new HttpHeaders().set('Content-Type', 'application/json');

    const changePasswordAfterSigninObject = {
      'username': username,
      'old_password': oldPassword,
      'new_password': newPassword,
      'stayLoggedIn': true,
      'sessionInformation': 'DatePoll-Mobile'
    };

    this.storageService.getValueWithKey('apiUrl').then((apiUrl) => {
      this.http.post(apiUrl + '/auth/changePasswordAfterSignin', changePasswordAfterSigninObject, {headers: headers}).subscribe(
        (data: any) => {
          console.log(data);
          this.showPasswordChangedToast();
          this.performLogin(data.token, data.sessionToken);
        }, (error) => console.log(error)
      );
    });
  }

  async showPasswordChangedToast() {
    const toast = await this.toastController.create({
      message: this.translate.getStringForValue('SIGNIN_TOAST_PASSWORD_CHANGED'),
      duration: 4000
    });
    toast.present();
  }
}
