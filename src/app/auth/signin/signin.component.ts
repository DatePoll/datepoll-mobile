import {Component, OnInit} from '@angular/core';
import {NgForm} from '@angular/forms';
import {HttpClient} from '@angular/common/http';
import {NavController, ToastController} from '@ionic/angular';

import {AuthService} from '../auth.service';
import {StorageService} from '../../shared/services/storage.service';
import {HttpService} from '../../shared/services/http.service';
import {TranslateService} from '../../shared/translation/translate.service';
import {LoadingService} from '../../shared/services/loading.service';

@Component({
  selector: 'app-signin',
  templateUrl: './signin.component.html',
  styleUrls: ['./signin.component.scss'],
})
export class SigninComponent implements OnInit {
  state = 'login';
  showLoadingBar = false;
  communityName = null;
  server: string;

  username: string;
  oldPassword: string;
  changePassword = '';
  changePasswordAgain = '';
  changePasswordNotEqual = true;

  constructor(
    private storageService: StorageService,
    private authService: AuthService,
    private http: HttpClient,
    private httpService: HttpService,
    private nav: NavController,
    private translate: TranslateService,
    private toastController: ToastController,
    public loadingService: LoadingService) {

    this.loadingService.present();
  }

  ngOnInit() {
    this.authService.checkIfSessionTokenExistsAsync().then((value) => {
      this.loadingService.dismiss();
      if (value != null) {
        this.authService.setSessionToken(value);
        this.httpService.refreshApiUrl();
        this.authService.refreshJWTToken('/home');
      }
    });
  }

  onServerChange() {
    if (this.server != null) {
      if (this.isValidUrl(this.server)) {
        this.http.get(this.server + '/api/settings/name').subscribe(
          (response: any) => {
            this.communityName = response.community_name;
          }
        );
      }
    }
  }

  isValidUrl(url): boolean {
    return !!((url.includes('.') || url.toLowerCase().includes('localhost'))
      && url.toLowerCase().includes('http') && url.includes('://'));
  }

  onSignin(form: NgForm) {
    this.showLoadingBar = true;
    const server = form.controls.server.value;

    if (!this.isValidUrl(server)) {
      this.showCheckServerAddressToast();
      this.showLoadingBar = false;
      return;
    }

    const username = form.controls.username.value;
    const password = form.controls.password.value;

    const apiUrl = server + '/api';
    this.storageService.setValueWithKey('apiUrl', apiUrl);
    console.log('signInComponent | API Url: ' + apiUrl);

    this.authService.signinUser(username, password, apiUrl).subscribe(
      (data: any) => {
        console.log(data);

        this.showLoadingBar = false;

        if (data.msg != null) {
          if (data.msg === 'notActivated') {
            this.state = data.msg;
          }

          if (data.msg === 'changePassword') {
            this.state = data.msg;
            this.username = username;
            this.oldPassword = password;
          }
          return;
        }

        this.authService.performLogin(data.token, data.sessionToken);
      },
      (error) => {
        console.log(error);
        this.showLoadingBar = false;

        if (error.status != null) {
          if (error.status === 404) {
            this.showNoConnectionToast();
          } else if (error.status === 400) {
            this.showUsernameOrPasswordIncorrectToast();
          } else {
            this.showNoConnectionToast();
          }
        }
      }
    );
  }

  onPasswordChange() {
    this.changePasswordNotEqual = this.changePassword !== this.changePasswordAgain;
  }

  onChangePassword(form: NgForm) {
    const password = form.value.password;

    this.authService.changePasswordAfterSignin(this.username, this.oldPassword, password);
  }

  async showNoConnectionToast() {
    const toast = await this.toastController.create({
      message: this.translate.getStringForValue('SIGNIN_TOAST_NO_CONNECTION'),
      duration: 4000
    });
    toast.present();
  }

  async showCheckServerAddressToast() {
    const toast = await this.toastController.create({
      message: this.translate.getStringForValue('SIGNIN_TOAST_CHECK_SERVER_ADDRESS'),
      duration: 4000
    });
    toast.present();
  }

  async showUsernameOrPasswordIncorrectToast() {
    const toast = await this.toastController.create({
      message: this.translate.getStringForValue('SIGNIN_TOAST_USERNAME_OR_PASSWORD_INCORRECT'),
      duration: 4000
    });
    toast.present();
  }
}
