import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {FormsModule} from '@angular/forms';
import {IonicModule} from '@ionic/angular';

import {AuthRoutingModule} from './auth-routing.module';
import {TranslationModule} from '../shared/translation/translation.module';

import {SigninComponent} from './signin/signin.component';


@NgModule({
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TranslationModule,
    AuthRoutingModule
  ],
  declarations: [
    SigninComponent
  ]
})
export class AuthModule {
}
